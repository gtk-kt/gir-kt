# Kotlin GIR generator

Please check out [gtk-kn](https://gitlab.com/gtk-kn/gtk-kn) a possible successor project.

## TODO
- [x] Build classes in decreasing reference count order
- [ ] Finish properly handling all type conversions
- [ ] Interface Implementation wrapping using internal private classes
- [ ] Ensure output builds in gtk-kt
