/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import org.jetbrains.kotlin.config.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

@Suppress("DSL_SCOPE_VIOLATION") //https://github.com/gradle/gradle/issues/22797
plugins {
	alias(libs.plugins.kotlin)
	alias(libs.plugins.kotlinx.serialization)
	application
	id("detekt-conventions")
}

group = "com.github.doomsdayrs.lib"
version = "0.1.0"

dependencies {
	implementation(libs.bundles.xmlutil)
	implementation(libs.kotlinpoet)
	implementation(libs.kotlinx.cli)
	testImplementation(kotlin("test"))
}

tasks.test {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions.jvmTarget = JvmTarget.JVM_11.description
}

tasks.withType<Wrapper> {
	description = "Regenerates the Gradle Wrapper files"
	distributionType = Wrapper.DistributionType.ALL
	gradleVersion = libs.versions.gradle.get()
}

application {
	mainClass.set("org.gtk.gir.kotlin.MainKt")
}
