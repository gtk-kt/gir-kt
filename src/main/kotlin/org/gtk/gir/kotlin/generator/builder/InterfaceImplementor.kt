/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.generator.type.cPointerFor
import org.gtk.gir.kotlin.model.gir.GIRImplements

/**
 * 01 / 02 / 2023
 */
fun TypeSpec.Builder.implementInterface(
	fileSpecBuilder: FileSpec.Builder,
	parentPointerName: String,
	implements: GIRImplements,
	kotlinPackageName: String,
) {
	val memoryType = CTypeMemory.find(implements.name)

	val className: ClassName = if (memoryType != null) {
		ClassName(
			memoryType.import,
			memoryType.name,
		)
	} else {
		ClassName(
			kotlinPackageName, // Assume it is in the same package, unbuilt
			implements.name,
		)
	}

	// TODO import ctype

	fileSpecBuilder.addImport(className.packageName, className.simpleName)

	addSuperinterface(className)

	if (memoryType != null) {
		addProperty(
			PropertySpec.builder(
				memoryType.name + "Pointer",
				cPointerFor(memoryType.cType),
				listOf(KModifier.OVERRIDE),
			)
				.initializer("$parentPointerName.reinterpret()")
				.build(),
		)
	}
}
