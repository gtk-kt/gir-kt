/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import org.gtk.gir.kotlin.generator.builder.TypeBuilder.Companion.addDescriptors
import org.gtk.gir.kotlin.generator.common.resolveCPackageName
import org.gtk.gir.kotlin.generator.common.resolveKotlinPackageName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRRepository
import java.io.File

/**
 * 09 / 11 / 2022
 *
 * Builds each Repository
 */
class RepositoryBuilder(
    private val repo: GIRRepository,
    private val repoDir: File
) {

	fun build() {
		val pack = repo.pack.firstOrNull()
		if (pack == null) {
			Log.error("Failed to build, no packages to work with")
			return
		}
		val packageName = resolveCPackageName(pack.name)
		val kotlinPackageName = resolveKotlinPackageName(pack.name)

		repo.namespace.forEach { nameSpace ->
			// IGNORE Constants
			// IGNORE Alias for now TODO alias in the future
			nameSpace.bitfields.forEach {
				CTypeMemory.add(
					CTypeMemory.ClassDescriptor(
						it.name,
						CTypeMemory.ClassType.DIRECT,
						kotlinPackageName,
						it.cType,
					),
				)
			}
			nameSpace.aliases.forEach {
				CTypeMemory.add(
					CTypeMemory.ClassDescriptor(
						it.name,
						CTypeMemory.ClassType.DIRECT,
						kotlinPackageName,
						it.cType,
					),
				)
			}

			val typeBuilders = nameSpace.callbacks.map {
				CallbackBuilder(repoDir, packageName, kotlinPackageName, it)
			} +
				nameSpace.enums.map {
					EnumBuilder(repoDir, packageName, kotlinPackageName, it)
				} +
				nameSpace.records.map {
					RecordBuilder(repoDir, packageName, kotlinPackageName, it)
				} +
				nameSpace.interfaces.map {
					InterfaceBuilder(repoDir, packageName, kotlinPackageName, it)
				} +
				nameSpace.classes.map {
					ClassBuilder(repoDir, packageName, kotlinPackageName, it)
				}
			typeBuilders.addDescriptors()
				.forEach {
					it.build()
				}
		}
	}
}
