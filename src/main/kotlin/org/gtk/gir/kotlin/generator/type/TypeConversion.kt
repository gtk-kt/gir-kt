/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.type

import org.gtk.gir.kotlin.exceptions.FunctionParameterException
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRArray
import org.gtk.gir.kotlin.model.gir.GIRParameter
import org.gtk.gir.kotlin.model.gir.GIRReturnValue
import org.gtk.gir.kotlin.model.gir.GIRType

/**
 * Generate a C to Kotlin conversion
 */
@Throws(FunctionParameterException::class)
@Suppress("LongMethod", "CyclomaticComplexMethod") // TODO fix this
fun generateCToKotlin(
	source: Any,
	array: GIRArray?,
	type: GIRType?,
	isNullable: Boolean = false,
	addImport: AddImportFunction
): String = buildString {
	append(if (isNullable) "?" else "!!")

	var foundMapping = false

	if (array != null) {
		when (array.type?.name) {
			"utf8" -> {
				addImport("org.gtk.glib", "toArray")
				append(".toArray()")
				foundMapping = true
			}
		}
	}
	if (type != null) {
		when (type.name) {
			// Object, while initialized early on, is needed by some classes before it.
			"Object" -> {
				addImport("org.gtk.gobject", "Object.wrap")
				append(".wrap()")
				foundMapping = true
			}

			"gboolean" -> {
				addImport("org.gtk.glib", "bool")
				append(".bool")
				foundMapping = true
			}

			"utf8" -> {
				addImport("kotlinx.cinterop", "toKString")
				append(".toKString()")
				foundMapping = true
			}

			// < -- we can ignore, it maps to kotlin automatically
			"guint" -> {
				foundMapping = true
			}

			"gint" -> {
				foundMapping = true
			}

			"double" -> {
				foundMapping = true
			}

			"gdouble" -> {
				foundMapping = true
			}

			"guint16" -> {
				foundMapping = true
			}

			"gunichar" -> {
				// TODO Verify
				foundMapping = true
			}

			"gfloat" -> {
				foundMapping = true
			}

			"gsize" -> {
				foundMapping = true
			}

			"gint64" -> {
				foundMapping = true
			}

			// -- >

			"none" -> {
				// TODO Investigate this
			}

			else -> {
				val searchName = type.cType ?: type.name
				if (searchName != null) {
					val classDescriptor =
						CTypeMemory.find(searchName)
							?: if (type.name != null)
								CTypeMemory.find(type.name)
							else null

					if (classDescriptor != null) {
						when (classDescriptor.type) {
							CTypeMemory.ClassType.ENUM -> {
								appendLine(".let { gtk -> ")
									.appendLine(classDescriptor.name + ".valueOf(gtk)")
									.append("}")
							}

							CTypeMemory.ClassType.DIRECT -> {
								// We can ignore translation, we do not need
								// to duplicate ints
							}

							CTypeMemory.ClassType.WRAPABLE -> {
								append(".wrap()")
							}

							CTypeMemory.ClassType.FUNCTION -> {
								throw FunctionParameterException()
							}
						}
						foundMapping = true
					} else {
						when (searchName) {
							"const char*" -> {
								append(".toKString()")
								foundMapping = true
							}
						}
					}
				}
			}
		}
	}
	if (!foundMapping) {
		Log.error("Cannot find mapping for $source")
	}
}

/**
 * Generate a c return parser.
 *
 * This works for any function with a normal return (no pointer setting, only a normal return).
 *
 * Expects cReturn and parsers it
 */
fun generateCReturnToKotlin(
	returnValue: GIRReturnValue,
	addImport: AddImportFunction
): String {
	val statement = StringBuilder("cReturn")
	statement.append(
		generateCToKotlin(
			returnValue,
			returnValue.array,
			returnValue.type,
			returnValue.nullable,
			addImport,
		),
	)
	return statement.toString()
}

/**
 * Parameter input parsing
 */
@Throws(FunctionParameterException::class)
@Suppress("NestedBlockDepth") // TODO fix this
fun generateKotlinToCParameter(
	parameter: GIRParameter,
	addImport: AddImportFunction
): String {
	val statement = StringBuilder(parameter.name)

	statement.append(if (parameter.nullable) "?" else "")

	/**
	 * Used to ensure only one check is made
	 */
	var matched = false;

	if (parameter.array != null) {
		when (parameter.array.type?.name) {
			"utf8" -> {
				addImport("org.gtk.glib", "toNullTermCStringArray")
				statement.append(".toNullTermCStringArray()")
			}
		}
		matched = true
	}

	if (!matched) {
		if (parameter.type != null) {
			if (parameter.type.name != null) {
				when (parameter.type.name) {
					"gboolean" -> {
						addImport("org.gtk.glib", "gtk")
						statement.append(".gtk")
					}

					"utf8" -> {}  // ignored: Kotlin Interop handles it

					else -> {
						val memory = if (parameter.type.cType != null) {
							CTypeMemory.find(parameter.type.cType)
								?: CTypeMemory.find(parameter.type.name)
						} else {
							CTypeMemory.find(parameter.type.name)
						}
						if (memory != null) {
							addImport(memory.import, memory.name)
							when (memory.type) {
								CTypeMemory.ClassType.WRAPABLE -> {
									addImport("kotlinx.cinterop", "reinterpret")
									statement.append(".pointer.reinterpret()")
								}

								CTypeMemory.ClassType.ENUM -> {
									statement.append(".value")
								}

								CTypeMemory.ClassType.DIRECT -> {
									// Leave as is
								}

								CTypeMemory.ClassType.FUNCTION -> {
									throw FunctionParameterException()
								}
							}
						} else {
							Log.error("generateKotlinToCParameter: No memory found for parameter $parameter")
						}
					}
				}
			} else {
				Log.error("generateKotlinToCParameter: parameter type name was null, $parameter")
			}
		} else {
			Log.error("generateKotlinToCParameter: parameter type was null, $parameter")
		}
	}

	return statement.toString()
}

typealias AddImportFunction = (packageName: String, name: String) -> Unit
