/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.model.gir.GIREnumeration
import org.gtk.gir.kotlin.model.gir.GIRMember
import java.io.File

/**
 * 20 / 11 / 2022
 */
class EnumBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val enumeration: GIREnumeration
) : TypeBuilder {
	override fun build() {
		repoDir.mkdirs()

		val lowercaseName = enumeration.name.lowercase()
		val pointerName = "${lowercaseName}Pointer"
		val enumOriginal = TypeVariableName(enumeration.cType)

		val fileSpec = FileSpec.builder(kotlinPackageName, enumeration.name).apply {
			this.indent("\t")
			addType(
				TypeSpec.enumBuilder(enumeration.name).apply {
					addImport(cPackageName, enumOriginal.name)
					enumeration.doc?.let { addKdoc("%L\n", it.text) }
					addConstructor(enumOriginal)
					enumeration.members.forEach { member: GIRMember ->
						addImport(cPackageName, enumOriginal.name + "." + member.cIdentifier)
						addEnumConstant(
							member.name.uppercase(),
							TypeSpec.anonymousClassBuilder()
								.addSuperclassConstructorParameter(
									member.cIdentifier,
								)
								.apply {
									member.doc?.let {
										addKdoc("%L\n", it.text)
									}
								}
								.build(),
						)
					}
					addProperty(
						PropertySpec.builder(pointerName, enumOriginal)
							.initializer(pointerName)
							.build(),
					)
					addCompanionObject(enumOriginal)
				}.build(),
			)
		}.build()

		fileSpec.writeTo(repoDir)
	}

	private fun TypeSpec.Builder.addCompanionObject(enumOriginal: TypeVariableName) {
		addType(
			TypeSpec.companionObjectBuilder()
				.apply {
					addFunction(
						FunSpec.builder("valueOf")
							.addParameter(ParameterSpec("gtk", enumOriginal))
							.returns(
								ClassName(
									"",
									enumeration.name,
								),
							)
							.beginControlFlow("return when(gtk)")
							.apply {
								enumeration.members.forEach { member: GIRMember ->
									val cId = member.cIdentifier
									val name = member.name.uppercase()

									addStatement(
										"$cId -> $name",
									)
								}
							}
							.endControlFlow()
							.build(),
					)
				}
				.build(),
		)
	}

	private fun TypeSpec.Builder.addConstructor(enumOriginal: TypeVariableName) {
		primaryConstructor(
			FunSpec.constructorBuilder().addParameter(
				"value",
				enumOriginal,
			).build(),
		)
	}

	override fun addDescriptor() {
		CTypeMemory.add(
			CTypeMemory.ClassDescriptor(
				enumeration.name,
				CTypeMemory.ClassType.ENUM,
				kotlinPackageName,
				enumeration.cType,
			),
		)
	}
}
