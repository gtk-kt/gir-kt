/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.common

import java.util.Locale

/*
 * 09 / 11 / 2022
 */

val snakeRegex = "_[a-zA-Z]".toRegex()
val dashRegex = "-[a-zA-Z]".toRegex()

fun String.snakeToCamelCase(): String = snakeRegex.replace(this) {
	it.value.replace("_", "")
		.uppercase(Locale.getDefault())
}

fun String.dashToCamelCase(): String = dashRegex.replace(this) {
	it.value.replace("-", "")
		.uppercase(Locale.getDefault())
}
