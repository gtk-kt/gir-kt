/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.UNIT
import org.gtk.gir.kotlin.exceptions.FunctionParameterException
import org.gtk.gir.kotlin.generator.type.AddImportFunction
import org.gtk.gir.kotlin.generator.common.asStablePointerClassName
import org.gtk.gir.kotlin.generator.type.generateKotlinToCParameter
import org.gtk.gir.kotlin.generator.type.resolveTypeName
import org.gtk.gir.kotlin.generator.common.snakeToCamelCase
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRMethod
import org.gtk.gir.kotlin.model.gir.GIRParameter

/**
 * 09 / 11 / 2022
 */
class MethodBuilder(
	private val kotlinPackage: String,
	private val pointerName: String,
	private val girMethod: GIRMethod,
	private val addImportFunction: AddImportFunction
) {

	fun build(builder: TypeSpec.Builder) {
		builder.addFunction(
			FunSpec.builder(girMethod.name.snakeToCamelCase()).apply {
				Log.info("Adding method ${girMethod.cIdentifier} ")

				if (girMethod.doc != null) addKdoc("%L\n", girMethod.doc.text)
				var hasFunctionParam = false
				var functionParm: GIRParameter? = null

				girMethod.parameters.parameters.forEach {
					try {
						generateKotlinToCParameter(it, addImportFunction)
					} catch (e: FunctionParameterException) {
						hasFunctionParam = true
						functionParm = it
					}
				}

				for (index in girMethod.parameters.parameters.indices) {
					val parameter = girMethod.parameters.parameters[index]

					if (hasFunctionParam) {
						if (parameter.name == functionParm!!.name)
							continue

						// Assume the last one is the data object for the parameter
						if (index == girMethod.parameters.parameters.size - 1 && parameter.name == "data" || parameter.name == "userdata" || parameter.name == "user_data")
							continue
					}

					addParameter(
						ParameterSpec.builder(
							parameter.name.snakeToCamelCase(),
							parameter.resolveTypeName(kotlinPackage),
						)
							.build(),
					)
				}

				var returns = false

				val className = girMethod.returnValue.resolveTypeName(kotlinPackage)
				if (className != UNIT) {
					returns(className.copy(nullable = girMethod.returnValue.nullable))
					returns = true
				}
				if (girMethod.returnValue.array != null) {
					returns = false
					// TODO handle returnArray for girMethod
				}

				val statementBuilder = StringBuilder("")

				if (returns) {
					statementBuilder.append("return ")
				}

				statementBuilder.append(girMethod.cIdentifier)

				statementBuilder.append("(")

				statementBuilder.append(pointerName)

				for (index in girMethod.parameters.parameters.indices) {
					val parameter = girMethod.parameters.parameters[index]

					statementBuilder.append(", ")
					if (hasFunctionParam) {
						if (index == girMethod.parameters.parameters.size - 1) {
							statementBuilder.append(functionParm!!.name.snakeToCamelCase())
								.append(".asStablePointer()")

							addImportFunction(
								asStablePointerClassName.packageName,
								asStablePointerClassName.simpleName,
							)
						}
						if (parameter.name == functionParm!!.name) {
							statementBuilder.append(
								"static" + parameter.type!!.name!!,
							)
						}
					} else {
						statementBuilder.append(
							generateKotlinToCParameter(parameter, addImportFunction),
						)
					}
				}

				statementBuilder.append(")")

				addStatement(
					"%L",
					statementBuilder.toString(),
				)
			}.build(),
		)
	}
}
