/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.generator.type.cPointerFor
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRRecord
import java.io.File

/**
 * 09 / 11 / 2022
 */
class RecordBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val record: GIRRecord
) : TypeBuilder {
	private val pointerTypeName = record.cType ?: record.glibTypeName ?: "TODO"

	override fun build() {
		Log.info("Adding class `${record.name}`")
		repoDir.mkdirs()

		val lowercaseName = record.name.lowercase()
		val pointerName = if (record.name == "Object") {
			"pointer"
		} else {
			"${lowercaseName}Pointer"
		}
		if (pointerTypeName == "TODO") {
			Log.error("Cannot resolve pointer type name for $record")
		}

		val pointerType = cPointerFor(pointerTypeName)

		val fileSpecBuilder = FileSpec.builder(kotlinPackageName, record.name)
		fileSpecBuilder.indent("\t")
		val typeSpecBuilder =
			getTypeSpecBuilder(fileSpecBuilder, pointerTypeName, pointerName, pointerType)
		fileSpecBuilder.addType(typeSpecBuilder)
		try {
			fileSpecBuilder.build().writeTo(repoDir)
		} catch (ignored: NoSuchElementException) {
		}
	}

	private fun getTypeSpecBuilder(
		fileSpecBuilder: FileSpec.Builder,
		pointerTypeName: String,
		pointerName: String,
		pointerType: TypeVariableName
	) = TypeSpec.classBuilder(record.name).apply {
		fileSpecBuilder.addImport("kotlinx.cinterop", "CPointer")
		fileSpecBuilder.addImport(cPackageName, pointerTypeName)

		record.doc?.let { addKdoc("%L\n", it.text) }

		primaryConstructor(
			FunSpec.constructorBuilder().addParameter(
				pointerName,
				pointerType,
			).build(),
		)

		addModifiers(KModifier.FINAL)

		addProperty(
			PropertySpec.builder(pointerName, pointerType).initializer(pointerName).build(),
		)

		// Import methods
		record.method
			.mapNotNull { it.cIdentifier }
			.forEach { fileSpecBuilder.addImport(cPackageName, it) }


		generateValMethods(kotlinPackageName, pointerName, record.method) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}

		generateMethods(kotlinPackageName, pointerName, record.method) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}

		val companionObj = getCompanionObject(pointerTypeName)

		addType(companionObj.build())
	}.build()

	private fun getCompanionObject(pointerTypeName: String) =
		TypeSpec.companionObjectBuilder()
			.apply {
				record.functions.forEach { girFunction ->
					val builder = FunctionBuilder(kotlinPackageName, girFunction)
					builder.build(this)
				}
				addFunction(
					FunSpec.builder("wrap")
						.receiver(
							cPointerFor(
								pointerTypeName,
							).copy(nullable = true),
						)
						.returns(ClassName("", record.name).copy(nullable = true))
						.addStatement("return·this?.wrap()")
						.build(),
				)
				addFunction(
					FunSpec.builder("wrap")
						.receiver(cPointerFor(pointerTypeName))
						.returns(ClassName("", record.name))
						.addStatement("return ${record.name}(this)")
						.build(),
				)
			}

	override fun addDescriptor() {
		CTypeMemory.add(
			CTypeMemory.ClassDescriptor(
				record.name,
				CTypeMemory.ClassType.WRAPABLE,
				kotlinPackageName,
				pointerTypeName,
			),
		)
	}
}
