/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.generator.type.cPointerFor
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRInterface
import java.io.File

/**
 * 09 / 11 / 2022
 */
class InterfaceBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val interfaze: GIRInterface
) : TypeBuilder {

	private val pointerTypeName = interfaze.cType ?: interfaze.glibTypeName
	private val pointerType = cPointerFor(pointerTypeName)

	override fun build() {
		Log.info("Adding interface `${interfaze.name}`")
		repoDir.mkdirs()

		val lowercaseName = interfaze.name.lowercase()
		val pointerName = if (interfaze.name == "Object") {
			"pointer"
		} else {
			"${lowercaseName}Pointer"
		}

		val fileSpecBuilder = FileSpec.builder(kotlinPackageName, interfaze.name)
		fileSpecBuilder.indent("\t")
		val typeSpecBuilder =
			getTypeSpecBuilder(fileSpecBuilder, pointerTypeName, pointerName, pointerType)
		fileSpecBuilder.addType(typeSpecBuilder)
		fileSpecBuilder.build().writeTo(repoDir)
	}

	private fun getTypeSpecBuilder(
		fileSpecBuilder: FileSpec.Builder,
		pointerTypeName: String,
		pointerName: String,
		pointerType: TypeVariableName
	) = TypeSpec.interfaceBuilder(interfaze.name).apply {
		fileSpecBuilder.addImport("kotlinx.cinterop", "CPointer")
		fileSpecBuilder.addImport(cPackageName, pointerTypeName)

		interfaze.doc?.let { addKdoc("%L\n", it.text) }

		addProperty(PropertySpec.builder(pointerName, pointerType).build())

		interfaze.implements.forEach { implements ->
			implementInterface(fileSpecBuilder, pointerName, implements, kotlinPackageName)
		}

		// Import methods
		interfaze.methods
			.mapNotNull { it.cIdentifier }
			.forEach { fileSpecBuilder.addImport(cPackageName, it) }

		generateVarMethods(
			interfaze.properties,
			kotlinPackageName,
			pointerName,
			interfaze.methods,
		) { packageName, names -> fileSpecBuilder.addImport(packageName, names) }


		generateValMethods(kotlinPackageName, pointerName, interfaze.methods) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}

		generateMethods(kotlinPackageName, pointerName, interfaze.methods) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}

		val companionObj = getCompanionObject(pointerTypeName)

		interfaze.signals.forEach { signal ->
			SignalBuilder(
				kotlinPackageName,
				fileSpecBuilder,
				interfaze.name,
				pointerType,
				this,
				companionObj,
				signal,
			).build()
		}

		addType(
			companionObj.build(),
		)
	}.build()

	private fun getCompanionObject(pointerTypeName: String) =
		TypeSpec.companionObjectBuilder()
			.apply {
				interfaze.functions.forEach { girFunction ->
					val builder = FunctionBuilder(kotlinPackageName, girFunction)
					builder.build(this)
				}
				addFunction(
					FunSpec.builder("wrap")
						.receiver(
							cPointerFor(
								pointerTypeName,
							).copy(nullable = true),
						)
						.returns(ClassName("", interfaze.name).copy(nullable = true))
						.addStatement("return·this?.wrap()")
						.build(),
				)
				addFunction(
					FunSpec.builder("wrap")
						.receiver(cPointerFor(pointerTypeName))
						.returns(ClassName("", interfaze.name))
						.addStatement("return ${interfaze.name}(this)")
						.build(),
				)
			}

	override fun addDescriptor() {
		CTypeMemory.add(
			CTypeMemory.ClassDescriptor(
				interfaze.name,
				CTypeMemory.ClassType.WRAPABLE,
				kotlinPackageName,
				pointerTypeName,
			),
		)
	}
}
