/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator

import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRRepository
import org.gtk.gir.kotlin.model.gir.GIRInclude
import nl.adaptivity.xmlutil.serialization.XML
import org.gtk.gir.kotlin.generator.builder.RepositoryBuilder
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import java.io.File

/**
 * 25 / 03 / 2022
 */
class Core(
	targetFilePath: String,
	outputDirectoryPath: String
) {
	private val xml = XML

	/**
	 * All the parsed repositories, distinct
	 */
	private val repos = mutableMapOf<String, GIRRepository>()

	/**
	 * How many times each repository is depended on
	 */
	private val counts = mutableMapOf<String, Int>().withDefault { 0 }

	private val mainFile = File(targetFilePath)
	private val outputDirectory = File(outputDirectoryPath)

	data class GirIncludeData(
		val name: String,
		val includes: List<GIRInclude>
	)

	private fun loopInclude(includes: List<GIRInclude>, depth: Int = 0) {
		includes.forEach { include: GIRInclude ->

			val tab = StringBuilder("").apply {
				if (depth > 0) {
					repeat(depth) { append("\t") }
				}
			}.toString()

			println("${tab}Parsing GIR of ${include.name}")

			val file = File(mainFile.parentFile, "${include.name}-${include.version}.gir")

			if (!file.exists()) {
				println(
					"${tab}WARNING: `${include.name}-${include.version}` " +
						"Does not have a GIR file, skipping.",
				)
				return@forEach
			}

			// Check if the repo was parsed already or not
			val repo =
				repos.getOrPut(include.name) { xml.decodeFromString(file.readText()) }
			println("${tab}Finished parsing GIR of `${include.name}`")

			// Save the work
			repos[include.name] = repo

			// Increment the count
			counts[include.name] = counts.getValue(include.name) + 1

			println("${tab}Parsing includes of `${include.name}`")
			loopInclude(repo.includes, depth + 1)
			println("${tab}Finished Parsing includes of `${include.name}`")
		}
	}

	fun execute() {
		println("Starting")

		println("Parsing org.gtk.gir.kotlin.main repo")

		val mainRepo = xml.decodeFromString<GIRRepository>(mainFile.readText())
		repos[mainRepo.pack.first().name] = mainRepo
		counts[mainRepo.pack.first().name] = 0

		println("Parsed org.gtk.gir.kotlin.main repo")

		println("Parsing includes of org.gtk.gir.kotlin.main repo")
		loopInclude(mainRepo.includes)
		println("Finished Parsing includes of org.gtk.gir.kotlin.main repo")

		println("Generating kotlin from repos from bottom to top")
		counts.entries.sortedByDescending { it.value }.map { it.key }.forEach { repoName ->
			/**
			 * Directory of this repo
			 */
			val repoDir = File(outputDirectory, repoName)
			repoDir.mkdirs()
			val repo = repos.getValue(repoName)
			Log.info("Building $repoName")
			val builder = RepositoryBuilder(repo, repoDir)
			builder.build()
		}
		Log.info("Errors: ${Log.errors.size}")
		Log.info("Warnings: ${Log.warnings.size}")
		Log.info("Dumping ctype memory")
		File(outputDirectory, "memory.txt").writeText(
			CTypeMemory.memory.joinToString(
				"\n",
				transform = { it.toString().replace("\n", " ") },
			),
		)

		println("Finished")
	}
}
