/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.TypeSpec
import org.gtk.gir.kotlin.generator.type.resolveTypeName
import org.gtk.gir.kotlin.generator.common.snakeToCamelCase
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRFunction

/**
 * 09 / 11 / 2022
 *
 * Builds a standalone function
 */
class FunctionBuilder(
	private val kotlinPackage: String,
	private val girFunction: GIRFunction
) {

	fun build(builder: TypeSpec.Builder) {
		Log.info("Adding function `${girFunction.name}`")
		// Ignore vararg functions
		if (girFunction.parameters?.parameters?.any { it.name == "..." } == true) {
			//Log.warning("Ignoring vararg function: ${girFunction.cIdentifier}")
			return
		}

		builder.addFunction(
			FunSpec.builder(girFunction.name.snakeToCamelCase()).apply {
				if (girFunction.doc != null) addKdoc("%L\n", girFunction.doc.text)

				girFunction.parameters?.parameters?.forEach {
					addParameter(
						ParameterSpec.builder(
							it.name.snakeToCamelCase(),
							it.resolveTypeName(kotlinPackage)
								.copy(nullable = it.nullable),
						)
							.build(),
					)
				}

				girFunction.returnValue
			}.build(),
		)
	}
}
