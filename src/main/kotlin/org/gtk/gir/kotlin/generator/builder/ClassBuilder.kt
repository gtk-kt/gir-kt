/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.generator.type.cPointerFor
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRClass
import java.io.File

/**
 * 09 / 11 / 2022
 */
class ClassBuilder(
	private val repoDir: File,
	private val cPackageName: String,
	private val kotlinPackageName: String,
	private val clazz: GIRClass
) : TypeBuilder {

	private val pointerTypeName = clazz.cType ?: clazz.glibTypeName
	private val pointerType = cPointerFor(pointerTypeName)

	override fun build() {
		Log.info("Adding class `${clazz.name}`")
		repoDir.mkdirs()

		val lowercaseName = clazz.name.lowercase()
		val pointerName = if (clazz.name == "Object") {
			"pointer"
		} else {
			"${lowercaseName}Pointer"
		}

		val fileSpecBuilder = FileSpec.builder(kotlinPackageName, clazz.name)
		fileSpecBuilder.indent("\t")
		val typeSpecBuilder =
			getTypeSpecBuilder(fileSpecBuilder, pointerTypeName, pointerName, pointerType)
		fileSpecBuilder.addType(typeSpecBuilder)
		fileSpecBuilder.build().writeTo(repoDir)
	}

	private fun getTypeSpecBuilder(
		fileSpecBuilder: FileSpec.Builder,
		pointerTypeName: String,
		pointerName: String,
		pointerType: TypeVariableName
	) = TypeSpec.classBuilder(clazz.name).apply {
		fileSpecBuilder.addImport("kotlinx.cinterop", "CPointer")
		fileSpecBuilder.addImport(cPackageName, pointerTypeName)

		clazz.doc?.let { addKdoc("%L\n", it.text) }

		primaryConstructor(
			FunSpec.constructorBuilder().addParameter(
				pointerName,
				pointerType,
			).build(),
		)

		if (!clazz.final) {
			addModifiers(KModifier.OPEN)
		}

		addProperty(
			PropertySpec.builder(pointerName, pointerType).initializer(pointerName).build(),
		)

		addSuperclassConstructor(fileSpecBuilder, pointerName)

		clazz.implements.forEach { implements ->
			implementInterface(fileSpecBuilder, pointerName, implements, kotlinPackageName)
		}

		// Import methods
		clazz.methods
			.mapNotNull { it.cIdentifier }
			.forEach { fileSpecBuilder.addImport(cPackageName, it) }

		generateVarMethods(
			clazz.properties,
			kotlinPackageName,
			pointerName,
			clazz.methods,
		) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}

		generateValMethods(kotlinPackageName, pointerName, clazz.methods) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}
		generateMethods(kotlinPackageName, pointerName, clazz.methods) { p, n ->
			fileSpecBuilder.addImport(p, n)
		}

		val companionObj = getCompanionObject(pointerTypeName)

		clazz.signals.forEach { signal ->
			SignalBuilder(
				kotlinPackageName,
				fileSpecBuilder,
				clazz.name,
				pointerType,
				this,
				companionObj,
				signal,
			).build()
		}

		addType(companionObj.build())
	}.build()

	private fun getCompanionObject(pointerTypeName: String) =
		TypeSpec.companionObjectBuilder()
			.apply {
				clazz.functions.forEach { girFunction ->
					val builder = FunctionBuilder(kotlinPackageName, girFunction)
					builder.build(this)
				}
				addFunction(
					FunSpec.builder("wrap")
						.receiver(
							cPointerFor(
								pointerTypeName,
							).copy(nullable = true),
						)
						.returns(ClassName("", clazz.name).copy(nullable = true))
						.addStatement("return·this?.wrap()")
						.build(),
				)
				addFunction(
					FunSpec.builder("wrap")
						.receiver(cPointerFor(pointerTypeName))
						.returns(ClassName("", clazz.name))
						.addStatement("return ${clazz.name}(this)")
						.build(),
				)
			}

	private fun TypeSpec.Builder.addSuperclassConstructor(
		fileSpecBuilder: FileSpec.Builder,
		pointerName: String
	) {
		if (clazz.parent != null) {
			val memoryType = CTypeMemory.find(clazz.parent)
			if (memoryType != null) {
				superclass(ClassName(memoryType.import, memoryType.name))
			} else {
				superclass(
					ClassName(
						kotlinPackageName, // Assume it is in the same package, unbuilt
						clazz.parent,
					),
				)
			}
			fileSpecBuilder.addImport("kotlinx.cinterop", "reinterpret")
			addSuperclassConstructorParameter("%L", "$pointerName.reinterpret()")
		}
	}

	override fun addDescriptor() {
		CTypeMemory.add(
			CTypeMemory.ClassDescriptor(
				clazz.name,
				CTypeMemory.ClassType.WRAPABLE,
				kotlinPackageName,
				pointerTypeName,
			),
		)
	}
}
