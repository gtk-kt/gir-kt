/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

interface TypeBuilder {

	/**
	 * Build the type
	 */
	fun build()

	/**
	 * Add descriptor to CTypeMemory
	 */
	fun addDescriptor()

	companion object {
		/**
		 * Compile a list of [TypeBuilder]s
		 */
		fun <T : TypeBuilder> List<T>.compile() =
			addDescriptors().forEach {
				it.build()
			}

		/**
		 * Add descriptors form a list of [TypeBuilder]s
		 */
		fun <T : TypeBuilder> List<T>.addDescriptors() =
			onEach {
				it.addDescriptor()
			}
	}
}
