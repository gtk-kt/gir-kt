/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.TypeSpec
import org.gtk.gir.kotlin.exceptions.FunctionParameterException
import org.gtk.gir.kotlin.generator.type.AddImportFunction
import org.gtk.gir.kotlin.generator.type.resolveTypeName
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRMethod
import org.gtk.gir.kotlin.model.gir.GIRProperty

/**
 * Creates var properties to add to a given type from a list of properties.
 * Based on glibGetProperty & glibSetProperty, if they match that means it should be a var.
 */
fun TypeSpec.Builder.generateVarMethods(
	properties: List<GIRProperty>,
	kotlinPackageName: String,
	pointerName: String,
	girMethods: List<GIRMethod>,
	addImportFunction: AddImportFunction
) {
	// Create var via their getter/setter combinations
	girMethods
		.filter { it.glibSetProperty != null || it.glibGetProperty != null }
		// group by their property names
		.groupBy { it.glibGetProperty ?: checkNotNull(it.glibSetProperty) }
		// only work with things that have 1 input / output
		.filter { propertyAccessors ->
			propertyAccessors.value.any { it.parameters.parameters.size <= 1 }
		}
		.forEach { (propertyName, methods) ->
			val property = properties.first { it.name == propertyName }
			val builder = PropertyBuilder(
				kotlinPackageName,
				pointerName,
				propertyName,
				property.resolveTypeName(kotlinPackageName),
				property.doc?.text,
				methods.find { it.glibGetProperty != null },
				methods.find { it.glibSetProperty != null },
				addImportFunction,
			)

			builder.build(this)
		}
}

/**
 * Generate val methods. Singular getter and setters.
 */
fun TypeSpec.Builder.generateValMethods(
	kotlinPackageName: String,
	pointerName: String,
	girMethods: List<GIRMethod>,
	addImportFunction: AddImportFunction
) {
	girMethods
		// only work with methods that are singular getter/setter
		.filter {
			it.parameters.parameters.size <= 1
		}
		// Ignore methods without names, tricky fellows
		.filter {
			it.name.isNotEmpty()
		}
		.forEach { girMethod ->
			/*
			Ignore vararg functions, there is no way to implement them due to
			 being compiling time dependent for C
			 */
			if (girMethod.parameters.parameters.any { it.name == "..." }) {
				//Log.warning("Ignoring vararg method: ${girMethod.cIdentifier}")
				return
			}


			Log.warning("Singular val $girMethod")
			val builder =
				PropertyBuilder(
					kotlinPackageName,
					pointerName,
					girMethod.name,
					girMethod.returnValue.resolveTypeName(kotlinPackageName),
					girMethod.doc?.text,
					girMethod,
					null,
					addImportFunction,
				)
			try {
				builder.build(this)
			} catch (ignored: FunctionParameterException) {
				/*
				We caught a function parameter exception.
				No worry, some GTK functions return the c function associated with certain things.
				 */
			}
		}
}

/**
 * Generate methods for a class.
 *
 * This will only generate methods that are not singular getter / setters.
 * Those should be generated as a "val".
 */
fun TypeSpec.Builder.generateMethods(
	kotlinPackageName: String,
	pointerName: String,
	girMethods: List<GIRMethod>,
	addImportFunction: AddImportFunction
) {
	girMethods
		// only work with methods that are not singular getter/setter
		.filter {
			it.glibGetProperty == null && it.glibSetProperty == null ||
				it.parameters.parameters.size > 1
		}
		// Ignore methods without names, tricky fellows
		.filter {
			it.name.isNotEmpty()
		}
		.forEach { girMethod ->
			/*
			Ignore vararg functions, there is no way to implement them due to
			 being compiling time dependent for C
			 */
			if (girMethod.parameters.parameters.any { it.name == "..." }) {
				//Log.warning("Ignoring vararg method: ${girMethod.cIdentifier}")
				return
			}

			val builder =
				MethodBuilder(kotlinPackageName, pointerName, girMethod, addImportFunction)
			builder.build(this)
		}
}
