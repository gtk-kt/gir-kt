/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.type

/**
 * 13 / 12 / 2022
 *
 * Contains the memory of what CTypes exist, useful for when a return type
 *  only tells us the CType and not what the CType is.
 */
object CTypeMemory {
	val memory = mutableListOf<ClassDescriptor>()

	init {
		addGlib("gboolean")
		addGlib("gdouble")
		addGlib("gfloat")
		addGlib("gint")
		addGlib("gint16")
		addGlib("gint32")
		addGlib("gint64")
		addGlib("gpointer")
		addGlib("gsize")
		addGlib("gssize")
		addGlib("guint")
		addGlib("guint16")
		addGlib("guint32")
		addGlib("guint64")
		addGlib("gunichar")

		addCType("const char*", "String")
	}

	fun find(search: String, recursed: Boolean = false): ClassDescriptor? =
		memory.find { it.cType == search } ?: memory.find { it.name == search }
		?: if (!recursed) find(
			search.substringAfter("."),
			true
		) else null

	fun add(classDescriptor: ClassDescriptor) {
		memory.add(classDescriptor)
	}

	private fun addGlib(name: String) {
		add(
			ClassDescriptor(
				name,
				ClassType.DIRECT,
				"glib",
				name,
			),
		)
	}

	/**
	 * Add a value that is associated with C, such as int.
	 */
	private fun addCType(cName: String, kotlinName: String) {
		add(
			ClassDescriptor(
				kotlinName,
				ClassType.DIRECT,
				"kotlin",
				cName,
			),
		)
	}

	enum class ClassType {
		ENUM,
		DIRECT,
		WRAPABLE,
		FUNCTION
	}

	data class ClassDescriptor(
		val name: String,
		val type: ClassType,
		val import: String,
		val cType: String
	)
}
