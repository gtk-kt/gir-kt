/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeAliasSpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import org.gtk.gir.kotlin.generator.common.addSignalCallbackClassName
import org.gtk.gir.kotlin.generator.common.asStableRefClassName
import org.gtk.gir.kotlin.generator.common.dashToCamelCase
import org.gtk.gir.kotlin.generator.common.gcallbackClassName
import org.gtk.gir.kotlin.generator.common.snakeToCamelCase
import org.gtk.gir.kotlin.generator.common.staticCFunctionClassName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.generator.type.generateCToKotlin
import org.gtk.gir.kotlin.generator.type.resolveCTypeName
import org.gtk.gir.kotlin.generator.type.resolveTypeName
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRDoc
import org.gtk.gir.kotlin.model.gir.GIRParameters
import org.gtk.gir.kotlin.model.gir.GIRSignal
import java.util.Locale

/**
 * 06 / 01 / 2023
 */
class SignalBuilder(
	private val kotlinPackage: String,
	private val fileBuilder: FileSpec.Builder,
	parentName: String,
	private val pointerType: TypeVariableName,
	private val classBuilder: TypeSpec.Builder,
	private val companion: TypeSpec.Builder,
	private val signal: GIRSignal
) {
	private val className: String = parentName

	fun build() {
		Log.info("Adding signal `${signal.name}`")
		fileBuilder.addImport("glib", "gpointer")
		val signalNameEmbed = signal.name.dashToCamelCase()
			.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

		val typeAliasName = "${className}${signalNameEmbed}Func"
		val staticFuncName = "static$typeAliasName"

		val funcName = "addOn${signalNameEmbed}Callback"

		addTypeAlias(typeAliasName, signal.parameters, funcName)

		fileBuilder.addImport(
			staticCFunctionClassName.packageName,
			staticCFunctionClassName.simpleName,
		)
		fileBuilder.addImport(
			asStableRefClassName.packageName,
			asStableRefClassName.simpleName,
		)
		addCompanionObject(staticFuncName, typeAliasName, funcName)

		fileBuilder.addImport(
			addSignalCallbackClassName.packageName,
			addSignalCallbackClassName.simpleName,
		)
		addFunction(funcName, signal.doc, staticFuncName, typeAliasName)
	}

	private fun addFunction(
        funcName: String,
        doc: GIRDoc?,
        staticFuncName: String,
        typeAliasName: String
	) {
		classBuilder.addFunction(
			FunSpec.builder(funcName).returns(ClassName("org.gtk.gobject", "SignalManager")).apply {
				if (doc != null) addKdoc("%L\n", doc.text)
				addKdoc("%L\n", "@see [$staticFuncName]")
				addKdoc("%L\n", "@see [$typeAliasName]")
			}.addParameter("callback", ClassName(kotlinPackage, typeAliasName)).addCode(
				CodeBlock.builder().apply {
					addStatement("return addSignalCallback(callback)")
				}.build(),
			).build(),
		)
	}

	@Suppress("LongMethod")  // TODO
	private fun addCompanionObject(
		staticFuncName: String,
		typeAliasName: String,
		funcName: String
	) {
		companion.addProperty(
			PropertySpec.builder(
				staticFuncName,
				gcallbackClassName,
			).initializer(
				CodeBlock.Builder().apply {
					beginControlFlow(
						buildString {
							append(staticCFunctionClassName.simpleName)
							append("{")
							append(" self : ${pointerType.name}")
							signal.parameters?.parameters?.forEach { parameter ->
								append(", ")
								append(parameter.name.snakeToCamelCase())
								append(": ")
								append(parameter.resolveCTypeName())
								parameter.type?.cType?.let { cType ->
									CTypeMemory.find(cType)?.let {
										fileBuilder.addImport(
											packageName = it.import,
											it.name,
										)
									}
								}
							}
							append(", data: gpointer")
							append(" ->")
						},
					)
					addStatement("%L", "data.asStableRef<$typeAliasName>()")
					addStatement("%L", "\t.get()")
					addStatement(
						"%L",
						buildString {
							append("\t.invoke(")
							append("self.wrap()")
							signal.parameters?.parameters?.forEach { param ->
								append(", ")
								append(param.name.snakeToCamelCase())
								append(
									generateCToKotlin(
										param,
										param.array,
										param.type,
										param.nullable,
									) { packageName, names ->
										fileBuilder.addImport(packageName = packageName, names)
									},
								)
							}
							append(")")
						},
					)
					endControlFlow()
					addStatement(".reinterpret()")
				}.build(),
			).apply {
				addModifiers(KModifier.PRIVATE)
				addKdoc("%L\n", "Static C function for [$className.$funcName]")
			}.build(),
		)
	}

	private fun addTypeAlias(typeAliasName: String, parameters: GIRParameters?, funcName: String) {
		fileBuilder.addTypeAlias(
			TypeAliasSpec.builder(
				typeAliasName,
				LambdaTypeName.get(
					receiver = ClassName(
						kotlinPackage,
						className,
					),
					parameters = parameters?.parameters?.map {
						ParameterSpec.unnamed(
							it.resolveTypeName(kotlinPackage),
						)
					}.orEmpty(),
					returnType = signal.returnValue.resolveTypeName(kotlinPackage),
				),
			).apply {
				addKdoc("%L\n", "Function type alias for [$className.$funcName]")
			}.build(),
		)
	}
}
