/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeAliasSpec
import org.gtk.gir.kotlin.exceptions.FunctionParameterException
import org.gtk.gir.kotlin.generator.common.asStableRefClassName
import org.gtk.gir.kotlin.generator.type.generateCToKotlin
import org.gtk.gir.kotlin.generator.type.resolveCTypeName
import org.gtk.gir.kotlin.generator.type.resolveTypeName
import org.gtk.gir.kotlin.generator.common.snakeToCamelCase
import org.gtk.gir.kotlin.generator.common.staticCFunctionClassName
import org.gtk.gir.kotlin.generator.type.CTypeMemory
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRCallback
import java.io.File

/**
 * Build a callback
 */
class CallbackBuilder(
	private val repoDir: File,
	private val packageName: String,
	private val kotlinPackageName: String,
	private val girCallback: GIRCallback
) : TypeBuilder {
	/**
	 * Represents the static function name
	 */
	private val staticName = "static" + girCallback.name

	override fun build() {
		Log.info("Adding class `${girCallback.name}`")
		repoDir.mkdirs()

		// Create typealias
		val fileSpecBuilder = FileSpec.builder(kotlinPackageName, girCallback.name)
		fileSpecBuilder.indent("\t")
		fileSpecBuilder.addTypeAlias(
			TypeAliasSpec.builder(
				girCallback.name,
				LambdaTypeName.get(
					parameters = girCallback.parameters?.parameters?.mapNotNull {
						// Ignore data parameters, these are used to pass the kotlin function into the c function
						if (it.name != "data")
							ParameterSpec.builder(
								it.name.snakeToCamelCase(),
								it.resolveTypeName(kotlinPackageName).copy(
									nullable = it.nullable
								),
							)
								.apply {
									if (it.doc != null)
										this.addKdoc("%L\n", it.doc.text)

								}
								.build()
						else null
					}.orEmpty(),
					returnType = girCallback.returnValue.resolveTypeName(kotlinPackageName),
				),
			).apply {
				if (girCallback.doc != null)
					addKdoc("%L\n", girCallback.doc.text)
			}.build(),
		)

		// Create static C function for the type alias
		val cTypeName = ClassName(packageName, girCallback.cType!!)
		fileSpecBuilder.addImport(cTypeName.packageName, cTypeName.simpleName)
		fileSpecBuilder.addProperty(
			PropertySpec.builder(
				staticName,
				cTypeName,
			).initializer(
				CodeBlock.Builder().apply {
					beginControlFlow(
						buildString {
							append(staticCFunctionClassName.simpleName)
							append("{")
							girCallback.parameters?.parameters?.forEachIndexed { index, parameter ->
								append(parameter.name.snakeToCamelCase())
								append(": ")
								append(parameter.resolveCTypeName())
								append("?") // Always presume null
								parameter.type?.cType?.let { cType ->
									CTypeMemory.find(cType)?.let {
										fileSpecBuilder.addImport(
											packageName = it.import,
											it.name,
										)
									}
								}
								if (index + 1 < girCallback.parameters.parameters.size) {
									append(", ")
								}
							}
							append(" ->")
						},
					)
					fileSpecBuilder.addImport(
						asStableRefClassName.packageName,
						asStableRefClassName.simpleName,
					)
					// Assume data is not null
					addStatement("%L", "data!!.asStableRef<${girCallback.name}>()")
					addStatement("%L", "\t.get()")
					addStatement(
						"%L",
						buildString {
							append("\t.invoke(")
							if (girCallback.parameters != null)
								for (index in girCallback.parameters.parameters.indices) {
									val param = girCallback.parameters.parameters[index]
									// Skip if the parameter is data
									if (param.name == "data") continue

									try {
										append(param.name.snakeToCamelCase())

										append(
											generateCToKotlin(
												param,
												param.array,
												param.type,
												param.nullable,
											) { packageName, names ->
												fileSpecBuilder.addImport(
													packageName = packageName,
													names,
												)
											},
										)
									} catch (e: FunctionParameterException) {
										this.removeSuffix(param.name.snakeToCamelCase())
										continue
									}

									if (index + 1 < girCallback.parameters.parameters.size) {
										append(", ")
									}
								}
							append(")")
						},
					)
					endControlFlow()
					addStatement(".reinterpret()")
				}.build(),
			).apply {
				addModifiers(KModifier.INTERNAL)
				if (girCallback.doc != null)
					addKdoc("%L\n", girCallback.doc.text)
			}.build(),
		)
		fileSpecBuilder.build().writeTo(repoDir)
	}

	override fun addDescriptor() {
		CTypeMemory.add(
			CTypeMemory.ClassDescriptor(
				girCallback.name,
				CTypeMemory.ClassType.FUNCTION,
				kotlinPackageName,
				girCallback.cType!!,
			),
		)

	}
}
