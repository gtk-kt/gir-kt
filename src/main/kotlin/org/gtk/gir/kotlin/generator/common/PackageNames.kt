/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.common

/**
 * 29 / 12 / 2022
 */
fun resolveKotlinPackageName(givenPackage: String): String =
	when (givenPackage) {
		"cairo-gobject" -> "org.cairographics.cairo"
		"gtk4" -> "org.gtk"
		"gdk-pixbuf-2.0" -> "org.gtk.gdk.pixbuf"
		"gio-2.0" -> "org.gtk.gio"
		"glib-2.0" -> "org.gtk.glib"
		"gmodule-2.0" -> "org.gtk.gmodule"
		"gobject-2.0" -> "org.gtk.gobject"
		"graphene-gobject-1.0" -> "io.github.ebassi.graphene"
		"harfbuzz-gobject" -> "io.github.harfbuzz.gobject"
		"pango" -> "org.gnome.pango"
		else -> givenPackage
	}

fun resolveCPackageName(givenPackage: String): String =
	when (givenPackage) {
		"cairo-gobject" -> "cairo"
		"gtk4" -> "gtk"
		"gdk-pixbuf-2.0" -> "pixbuf"
		"gio-2.0" -> "gio"
		"glib-2.0" -> "glib"
		"gmodule-2.0" -> "gmodule"
		"gobject-2.0" -> "gobject"
		"graphene-gobject-1.0" -> "graphene"
		"harfbuzz-gobject" -> "harfbuzz"
		"pango" -> givenPackage
		else -> givenPackage
	}
