/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.type

import com.squareup.kotlinpoet.ARRAY
import com.squareup.kotlinpoet.BOOLEAN
import com.squareup.kotlinpoet.CHAR
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.DOUBLE
import com.squareup.kotlinpoet.FLOAT
import com.squareup.kotlinpoet.INT
import com.squareup.kotlinpoet.LONG
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.SHORT
import com.squareup.kotlinpoet.STRING
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.TypeVariableName
import com.squareup.kotlinpoet.UNIT
import com.squareup.kotlinpoet.U_INT
import com.squareup.kotlinpoet.U_LONG
import com.squareup.kotlinpoet.U_SHORT
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRArray
import org.gtk.gir.kotlin.model.gir.GIRInstanceParameter
import org.gtk.gir.kotlin.model.gir.GIRParameter
import org.gtk.gir.kotlin.model.gir.GIRProperty
import org.gtk.gir.kotlin.model.gir.GIRReturnValue
import org.gtk.gir.kotlin.model.gir.GIRType

/**
 * Get a [TypeName] that serves as an Alias for [this] [String].
 *
 * If no found mapping exists, will assume it exists in the [defaultPackage].
 *
 * For example,
 * `gboolean` will return [BOOLEAN]
 */
fun String.getKotlinClass(defaultPackage: String): TypeName =
	when (this) {
		"gboolean" -> BOOLEAN

		// int
		"gint" -> INT
		"gint16" -> SHORT
		"gint32" -> INT
		"gint64" -> LONG

		// unsigned integers
		"guint" -> U_INT
		"guint16" -> U_SHORT
		"guint32" -> U_INT
		"guint64" -> U_LONG

		"gsize" -> U_LONG

		"utf8" -> STRING

		"gchar" -> CHAR

		"gdouble" -> DOUBLE
		"gfloat" -> FLOAT

		"gpointer" -> {
			TypeVariableName(
				"CPointer<out kotlinx.cinterop.CPointed>",
			)
		}

		"none" -> UNIT
		"const char*" -> STRING
		else -> {
			ClassName(
				defaultPackage,
				this.substringAfter("."), // Some names like being glib.Blah
			)
		}
	}

/**
 * Resolve the [TypeName] appropriate for this property
 */
fun GIRProperty.resolveTypeName(defaultPackage: String): TypeName =
	resolveTypeName(this, name, type, array, defaultPackage)

/**
 * Resolve the [TypeName] appropriate for this return value
 */
fun GIRReturnValue.resolveTypeName(defaultPackage: String): TypeName =
	resolveTypeName(this, null, type, array, defaultPackage)

/**
 * Resolve the [TypeName] appropriate for this parameter
 */
fun GIRParameter.resolveTypeName(defaultPackage: String): TypeName =
	resolveTypeName(this, name, type, array, defaultPackage)

/**
 * Used to create a static c function.
 *
 * Resolve the c type name to use.
 */
fun GIRParameter.resolveCTypeName(): String =
	resolveCTypeName(this, type)

fun GIRInstanceParameter.resolveCTypeName() =
	resolveCTypeName(this, type)

fun resolveCTypeName(parent: Any, type: GIRType?): String {
	if (type != null) {
		if (type.cType != null) {
			return when (type.cType) {
				"int" -> "Int"
				else -> type.cType
			}
		}

		if (type.name != null) {
			CTypeMemory.find(type.name)?.let { memory ->
				return memory.cType
			}
		}
	}
	Log.error("Failed to resolve CTypeName for $parent")
	return "TODO()"
}

fun resolveTypeName(
	parent: Any,
	name: String?,
	type: GIRType?,
	array: GIRArray?,
	defaultPackage: String
): TypeName {
	if (type != null) {
		if (type.name != null) {
			if (type.name == name) {
				//Log.warning("Type has same name as parent, investigate $parent")

				// So we can assume all "filename" parameters have broken gir documentation
				// cType == "const char*"
				return type.cType!!.getKotlinClass(defaultPackage)
			}
			return type.name.getKotlinClass(defaultPackage)
		} else {
			Log.warning("Type has no name, investigate $parent")
		}
	}

	if (array != null) {
		if (array.type != null) {
			// this is a primitive array

			if (array.type.name != null) {
				val arrayType = array.type.name.getKotlinClass(defaultPackage)
				return ARRAY.parameterizedBy(arrayType)
			} else {
				Log.warning("Type has no name, investigate $parent")
			}
		} else {
			// this is an object array
			return UNIT
		}
	}

	error("Cannot resolve type name")
}
