/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.generator.builder

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.TypeSpec
import org.gtk.gir.kotlin.generator.common.dashToCamelCase
import org.gtk.gir.kotlin.generator.type.AddImportFunction
import org.gtk.gir.kotlin.generator.type.generateCReturnToKotlin
import org.gtk.gir.kotlin.generator.type.generateKotlinToCParameter
import org.gtk.gir.kotlin.generator.type.resolveTypeName
import org.gtk.gir.kotlin.logging.Log
import org.gtk.gir.kotlin.model.gir.GIRMethod

/**
 * 09 / 11 / 2022
 *
 * Create a "property" to use
 */
class PropertyBuilder(
	private val kotlinPackage: String,
	private val pointerName: String,

	private val propertyName: String,
	private val propertyClass: TypeName,
	private val doc: String?,
	private val getMethod: GIRMethod?,
	private val setMethod: GIRMethod?,
	private val addImportFunction: AddImportFunction
) {

	fun build(builder: TypeSpec.Builder) {
		Log.info("Adding property `$propertyName` with class `$propertyClass`")
		if (propertyName == "license-type") {
			println("mark")
		}

		builder.addProperty(
			PropertySpec.builder(propertyName.dashToCamelCase(), propertyClass)
				.apply {
					if (doc != null)
						addKdoc("%L\n", doc)

					if (getMethod != null)
						getMethod(getMethod)

					setMethod(setMethod)
				}
				.build(),
		)
	}

	private fun PropertySpec.Builder.getMethod(getMethod: GIRMethod) {
		Log.info("Adding method ${getMethod.cIdentifier} ")
		getter(
			FunSpec.getterBuilder().apply {
				if (getMethod.doc != null) {
					addKdoc(
						"%L\n",
						getMethod.doc.text,
					)
				}

				addStatement(
					"val cReturn = %L",
					"${getMethod.cIdentifier}($pointerName)",
				)

				addStatement(
					"return %L",
					generateCReturnToKotlin(
						getMethod.returnValue,
						addImportFunction,
					),
				)
			}
				.build(),
		)
	}

	private fun PropertySpec.Builder.setMethod(setMethod: GIRMethod?) {
		mutable(setMethod != null)
		if (setMethod != null) {
			Log.info("Adding method ${setMethod.cIdentifier} ")
			val setMethodParam = setMethod.parameters.parameters.first()
			setter(
				FunSpec.setterBuilder()
					.apply {
						if (setMethod.doc != null) {
							addKdoc(
								"%L\n",
								setMethod.doc.text,
							)
						}

						addParameter(
							ParameterSpec.builder(
								setMethodParam.name,
								setMethodParam.resolveTypeName(kotlinPackage),
							).build(),
						)

						addStatement(
							"%L",
							(setMethod.cIdentifier ?: "null") +
								"($pointerName" + ", " +
								generateKotlinToCParameter(
									setMethodParam,
									addImportFunction,
								) +
								")",
						)
					}
					.build(),
			)
		}
	}
}
