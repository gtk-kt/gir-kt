/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.logging

import java.io.File

/**
 * 13 / 09 / 2022
 */
object Log {
	val warnings = ArrayList<Any?>()
	val errors = ArrayList<Any?>()
	private var mainLog: File? = null
	private var errorLog: File? = null
	private var warningLog: File? = null
	private const val CRESET: String = "\u001B[0m"
	private const val CRED: String = "\u001B[31m"
	private const val CGREEN: String = "\u001B[32m"
	private const val CYELLOW: String = "\u001B[15m"

	fun warning(message: Any?) {
		val warningMessage = "Warning:\t$message"
		warnings.add(warningMessage)
		listOf(mainLog, warningLog).forEach { checkNotNull(it).appendText(warningMessage + "\n\n") }
		println(CYELLOW + warningMessage + CRESET)
	}

	fun error(message: Any?) {
		val errorMessage = "Error:\t$message"
		errors.add(errorMessage)
		listOf(mainLog, errorLog).forEach { checkNotNull(it).appendText(errorMessage + "\n\n") }
		println(CRED + errorMessage + CRESET)
	}

	fun info(message: Any?) {
		val infoMessage = "Info:\t$message"
		checkNotNull(mainLog).appendText(infoMessage + "\n\n")
		println(CGREEN + infoMessage + CRESET)
	}

	fun setOut(outputPath: String) {
		mainLog = File(outputPath, "build.log").also { file ->
			val parentFile = file.parentFile
			if (!parentFile.exists() && !parentFile.mkdirs()) {
				kotlin.error("Failed to create directories: $parentFile")
			}
			if (!file.exists() && !file.createNewFile()) {
				kotlin.error("Failed to create file: $file")
			}
			file.writeText("")
		}
		errorLog = File(outputPath, "errors.log").apply { writeText("") }
		warningLog = File(outputPath, "warnings.log").apply { writeText("") }
	}
}
