/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS
import org.gtk.gir.kotlin.model.XMLNS_C

/**
 * 23 / 02 / 2022
 */

@Serializable
@XmlSerialName("repository", XMLNS, "")
data class GIRRepository(
	val version: String?,

	@XmlSerialName("identifier-prefixes", XMLNS_C, "c")
	val cIdentifierPrefixes: String?,

	@XmlSerialName("identifier-prefixes", XMLNS_C, "c")
	val cSymbolPrefixes: String?,

	val includes: List<GIRInclude>,

	val cInclude: List<GIRCInclude>,

	val pack: List<GIRPackage>,

	val namespace: List<GIRNameSpace>
)
