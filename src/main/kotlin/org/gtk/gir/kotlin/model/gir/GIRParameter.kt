/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS

@Serializable
@XmlSerialName("parameter", XMLNS, "")
data class GIRParameter(
	val name: String,

	@XmlSerialName("transfer-ownership", "", "")
	val transferOwnership: String?,

	val type: GIRType?,

	val nullable: Boolean = false,

	@XmlSerialName("allow-none", "", "")
	val allowNone: Boolean = false,

	val closure: Int?,

	val scope: String?,
	val destroy: Int?,

	val array: GIRArray?,

	val varargs: GIRVarArgs?,

	val direction: String?,

	@XmlSerialName("caller-allocates", "", "")
	val callerAllocates: Boolean = false,

	val optional: Boolean = false,
	override val `doc-version`: GIRDocVersion?,
	override val `doc-stability`: GIRDocStability?,
	override val doc: GIRDoc?,
	override val `doc-deprecated`: GIRDocDeprecated?,
	override val `source-position`: GIRSourcePosition?,
) : GIRDocElements
