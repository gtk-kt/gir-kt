/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS
import org.gtk.gir.kotlin.model.XMLNS_C

@Serializable
@XmlSerialName("namespace", XMLNS, "")
data class GIRNameSpace(
	val docsection: GIRDocSection?, // TODO Unknown

	val name: String?,

	val version: String?,

	@XmlSerialName("identifier-prefixes", XMLNS_C, "c")
	val cIdentifierPrefixes: String?,

	@XmlSerialName("symbol-prefixes", XMLNS_C, "c")
	val cSymbolPrefixes: String?,

	@XmlSerialName("prefix", XMLNS_C, "c")
	val cPrefix: String?,

	@SerialName("shared-library")
	val sharedLibrary: String?,

	val aliases: List<GIRAlias> = emptyList(),

	val classes: List<GIRClass> = emptyList(),

	val interfaces: List<GIRInterface> = emptyList(),

	val records: List<GIRRecord> = emptyList(),

	val enums: List<GIREnumeration> = emptyList(),

	val functions: List<GIRFunction> = emptyList(),

	val unions: List<GIRUnion> = emptyList(),

	val bitfields: List<GIRBitfield> = emptyList(),

	val callbacks: List<GIRCallback> = emptyList(),

	val constants: List<GIRConstant> = emptyList(),

	val annotations: List<GIRAnnotation> = emptyList(),

	val boxes: List<GIRBoxed> = emptyList(),

	val functionMacros: List<GIRFunctionMacro> = emptyList(),
)
