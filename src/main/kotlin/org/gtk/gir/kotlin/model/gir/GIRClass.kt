/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS
import org.gtk.gir.kotlin.model.XMLNS_C
import org.gtk.gir.kotlin.model.XMLNS_GLIB

@Serializable
@XmlSerialName("class", XMLNS, "")
data class GIRClass(
	val name: String,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String,

	val parent: String?,

	@XmlSerialName("type-struct", XMLNS_GLIB, "glib")
	val glibTypeStruct: String?,

	@XmlSerialName("ref-func", XMLNS_GLIB, "glib")
	val glibRefFunc: String?,

	@XmlSerialName("unref-func", XMLNS_GLIB, "glib")
	val glibUnRefFunc: String?,

	@XmlSerialName("set-value-func", XMLNS_GLIB, "glib")
	val glibSetValueFunc: String?,

	@XmlSerialName("get-value-func", XMLNS_GLIB, "glib")
	val glibGetValueFunc: String?,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String,

	val abstract: Boolean = false,

	@XmlSerialName("fundamental", XMLNS_GLIB, "glib")
	val glibFundamental: Boolean = false,

	val final: Boolean = false,

	override val `doc-version`: GIRDocVersion?,
	override val `doc-stability`: GIRDocStability?,
	override val doc: GIRDoc?,
	override val `doc-deprecated`: GIRDocDeprecated?,
	override val `source-position`: GIRSourcePosition?,
	override val annotations: List<GIRAnnotation>,

	val implements: List<GIRImplements> = emptyList(),

	val constructor: List<GIRConstructor> = emptyList(),

	val methods: List<GIRMethod>,

	val functions: List<GIRFunction> = emptyList(),

	val virtualMethods: List<GIRVirtualMethod> = emptyList(),

	val fields: List<GIRField> = emptyList(),

	val properties: List<GIRProperty>,

	val signals: List<GIRSignal>,

	val unions: List<GIRUnion> = emptyList(),

	val constants: List<GIRConstant> = emptyList(),

	val records: List<GIRRecord> = emptyList(),

	val callbacks: List<GIRCallback> = emptyList(),

	val sourcePosition: GIRSourcePosition?,

	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	) : GIRInfoAttrs, GIRInfoElements
