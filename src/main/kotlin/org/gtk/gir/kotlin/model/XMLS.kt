/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model

/*
 * 23 / 03 / 2022
 */
const val XMLNS = "http://www.gtk.org/introspection/core/1.0"
const val XMLNS_C = "http://www.gtk.org/introspection/c/1.0"
const val XMLNS_GLIB = "http://www.gtk.org/introspection/glib/1.0"
const val XMLNS_XML = "http://www.w3.org/XML/1998/namespace"
