/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS
import org.gtk.gir.kotlin.model.XMLNS_C
import org.gtk.gir.kotlin.model.XMLNS_GLIB

@Serializable
@XmlSerialName("interface", XMLNS, "")
data class GIRInterface(

	val name: String,

	@XmlSerialName("type-name", XMLNS_GLIB, "glib")
	val glibTypeName: String,

	@XmlSerialName("get-type", XMLNS_GLIB, "glib")
	val glibGetType: String,

	@XmlSerialName("symbol-prefix", XMLNS_C, "c")
	val cSymbolPrefix: String?,

	@XmlSerialName("type", XMLNS_C, "c")
	val cType: String?,

	@XmlSerialName("type-struct", XMLNS_GLIB, "glib")
	val glibTypeStruct: String? = null,

	override val `doc-version`: GIRDocVersion?,
	override val `doc-stability`: GIRDocStability?,
	override val doc: GIRDoc?,
	override val `doc-deprecated`: GIRDocDeprecated?,
	override val `source-position`: GIRSourcePosition?,
	override val annotations: List<GIRAnnotation>,

	val prerequisites: List<GIRPrerequisite> = emptyList(),

	val implements: List<GIRImplements> = emptyList(),

	val functions: List<GIRFunction> = emptyList(),

	val constructors: List<GIRConstructor> = emptyList(),

	val methods: List<GIRMethod> = emptyList(),

	val virtualMethods: List<GIRVirtualMethod> = emptyList(),

	val fields: List<GIRField> = emptyList(),

	val properties: List<GIRProperty> = emptyList(),

	val signals: List<GIRSignal> = emptyList(),

	val callbacks: List<GIRCallback> = emptyList(),

	val constants: List<GIRConstant> = emptyList(),

	val sourcePosition: GIRSourcePosition?,

	override val introspectable: Boolean?,

	override val deprecated: Boolean?,

	override val `deprecated-version`: String?,

	override val version: String?,

	override val stability: String?,

	) : GIRInfoAttrs, GIRInfoElements
