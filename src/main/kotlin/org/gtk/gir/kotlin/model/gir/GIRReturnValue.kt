/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS

@Serializable
@XmlSerialName("return-value", XMLNS, "")
data class GIRReturnValue(
	val introspectable: Boolean = false,

	val nullable: Boolean = false,

	val closure: Int?,

	val scope: GIRScope?,

	val destroy: Int?,

	val skip: Boolean = false,

	@Deprecated("Replaced by nullable & optional")
	val `allow-none`: Boolean = false,

	val `transfer-ownership`: String?,

	val type: GIRType?,

	val array: GIRArray?,

	val attribute: GIRAnnotation?,
	override val `doc-version`: GIRDocVersion?,
	override val `doc-stability`: GIRDocStability?,
	override val doc: GIRDoc?,
	override val `doc-deprecated`: GIRDocDeprecated?,
	override val `source-position`: GIRSourcePosition?
) : GIRDocElements
