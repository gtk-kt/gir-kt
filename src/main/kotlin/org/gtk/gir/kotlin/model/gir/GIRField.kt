/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.gtk.gir.kotlin.model.gir

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlSerialName
import org.gtk.gir.kotlin.model.XMLNS

@Serializable
@XmlSerialName("field", XMLNS, "")
data class GIRField(
	override val introspectable: Boolean?,
	override val deprecated: Boolean?,
	override val `deprecated-version`: String?,
	override val version: String?,
	override val stability: String?,

	val name: String,

	val writable: Boolean = false,

	val readable: Boolean = false,

	val private: Boolean = false,

	val bits: Int?,

	val type: GIRType?,

	val callback: GIRCallback?,

	val array: GIRArray? = null,
	override val `doc-version`: GIRDocVersion?,
	override val `doc-stability`: GIRDocStability?,
	override val doc: GIRDoc?,
	override val `doc-deprecated`: GIRDocDeprecated?,
	override val `source-position`: GIRSourcePosition?,
	override val annotations: List<GIRAnnotation>,
) : GIRInfoAttrs, GIRInfoElements
