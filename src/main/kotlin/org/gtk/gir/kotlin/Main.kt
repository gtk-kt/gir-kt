/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.gtk.gir.kotlin

import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import org.gtk.gir.kotlin.generator.Core
import org.gtk.gir.kotlin.logging.Log
import java.io.File
import kotlin.system.exitProcess

fun main(args: Array<String>) {
	val parser = ArgParser("gir-kt")
	val girPath by parser
		.option(
			ArgType.String,
			shortName = "g",
			description = "Path to the GIR file",
		)
		.default("/usr/share/gir-1.0/Gtk-4.0.gir")
	val outputPath by parser
		.option(
			ArgType.String,
			shortName = "o",
			description = "Output directory",
		)
		.default("build/gir-kt/")
	parser.parse(args)

	Log.setOut(outputPath)

	val file = File(girPath)
	if (!file.exists()) {
		Log.error("Error: the specified file does not exist.")
		exitProcess(2)
	}

	Core(
		girPath,
		outputPath,
	).execute()
}
