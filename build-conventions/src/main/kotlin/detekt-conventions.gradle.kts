/*
 *  Copyright (c) 2023 gir-kt
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import org.gradle.accessors.dm.LibrariesForLibs

plugins {
	id("io.gitlab.arturbosch.detekt")
}

val libs = the<LibrariesForLibs>()

detekt {
	toolVersion = libs.versions.detekt.get()
	source = files("src/main/kotlin", "src/test/kotlin", "src/androidTest/kotlin")
	parallel = true
	autoCorrect = true
}

dependencies {
	detektPlugins(libs.detekt)
}

afterEvaluate {
	tasks.named("check").configure {
		dependsOn(tasks.named("detektMain"))
		dependsOn(tasks.named("detektTest"))
	}
}
